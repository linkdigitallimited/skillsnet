<?php
$params   = array(
	'posts_per_page' => 1,
	'post_type'      => 'product',
	'orderby'        => 'menu-order',
	'order'          => 'desc',
	'fields'         => 'ids',
	'tax_query'      =>
	array(
		array(
			'taxonomy' => 'product_visibility',
			'field'    => 'term_id',
			'terms'    => 7,
			'operator' => 'NOT IN',
		),
	),
);
$wc_query = new WP_Query( $params );

if ( $wc_query->have_posts() ) :
	while ( $wc_query->have_posts() ) :
		$wc_query->the_post();
		get_template_part( 'template-parts/tiles/homepage' );
	endwhile;
else :
	// Insert any content or load a template for no posts found.
	echo 'No posts found';
endif;

wp_reset_query();
?>
<div class="tile-grid-secondary">
	<?php
	$params   = array(
		'posts_per_page' => 3,
		'post_type'      => 'product',
		'orderby'        => 'menu-order',
		'order'          => 'desc',
		'fields'         => 'ids',
		'offset'         => 1,
		'tax_query'      =>
			array(
				array(
					'taxonomy' => 'product_visibility',
					'field'    => 'term_id',
					'terms'    => 7,
					'operator' => 'NOT IN',
				),
			),
	);
	$wc_query = new WP_Query( $params );
	if ( $wc_query->have_posts() ) :
		while ( $wc_query->have_posts() ) :
			$wc_query->the_post();
			get_template_part( 'template-parts/tiles/homepage-secondary' );
		endwhile;
	else :
		// Insert any content or load a template for no posts found.
		echo 'No posts found';
	endif;
	wp_reset_query();
