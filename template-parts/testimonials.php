<?php
/**
 * Template for displaying Testimonials
 */
?>

<div class="testimonials-holder">
	<h2 class="section-title testimonial-title"><?php echo get_sub_field( 'title' ); ?></h2>
	<div class="testimonials-grid">
		<?php
		$testimonials = new WP_Query(
			array(
				'post_type'        => 'testimonials',
				'posts_per_page'   => 3,
				'orderby'          => 'rand',
				'tax_query'        => array(
					array(
						'taxonomy' => 'testimonial_category',
						'terms'    => get_sub_field( 'type' ),
					),
				),
			)
		);
		if ( $testimonials->have_posts() ) :
			$i = 1;
			while ( $testimonials->have_posts() ) :
				$testimonials->the_post();
				echo "<div class='testimonial-item testimonial-item-$i'>";
				get_template_part( 'template-parts/testimonials/item' );
				echo '</div>';
				$i++;
		endwhile;
		else :
			// Insert any content or load a template for no posts found.
		endif;

		wp_reset_query();
		?>
	</div>
</div>
