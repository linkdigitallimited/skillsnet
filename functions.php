<?php
/**
 * skillsnet functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package skillsnet
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'skillsnet_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function skillsnet_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on skillsnet, use a find and replace
		 * to change 'skillsnet' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'skillsnet', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1'   => esc_html__( 'Primary', 'skillsnet' ),
				'menu-2'   => esc_html__( 'Sidebar', 'skillsnet' ),
				'footer-1' => esc_html__( 'Footer Navigation', 'skillsnet' ),
				'footer-2' => esc_html__( 'Footer Policies', 'skillsnet' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'skillsnet_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'skillsnet_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function skillsnet_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'skillsnet_content_width', 640 );
}
add_action( 'after_setup_theme', 'skillsnet_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function skillsnet_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'skillsnet' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'skillsnet' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'skillsnet_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function skillsnet_scripts() {
	wp_deregister_script( 'jquery' );
	wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), null, true );
	wp_enqueue_style( 'skillsnet-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_enqueue_style( 'font_awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css', array(), _S_VERSION );
	wp_enqueue_style( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', array(), _S_VERSION );
	wp_style_add_data( 'skillsnet-style', 'rtl', 'replace' );

	wp_enqueue_script( 'skillsnet-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'slick-js', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'skillsnet_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

function testimonials() {
	$labels = array(
		'name'               => _x( 'Testimonials', 'testimonials' ),
		'singular_name'      => _x( 'Testimonial', 'testimonial' ),
		'add_new'            => _x( 'Add New', 'testimonial' ),
		'add_new_item'       => __( 'Add New Testimonial' ),
		'edit_item'          => __( 'Edit Testimonial' ),
		'new_item'           => __( 'New Testimonial' ),
		'all_items'          => __( 'All Testimonials' ),
		'view_item'          => __( 'View Testimonials' ),
		'search_items'       => __( 'Search Testimonials' ),
		'not_found'          => __( 'No Testimonials found' ),
		'not_found_in_trash' => __( 'No Testimonials found in the Trash' ),
		'parent_item_colon'  => '',
		'menu_name'          => 'Testimonials',
	);
	$args   = array(
		'labels'        => $labels,
		'description'   => 'Holds our Testimonials',
		'public'        => true,
		'menu_icon'     => 'dashicons-welcome-add-page',
		'menu_position' => 6,
		'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'author' ),
		'has_archive'   => true,
	);
	register_post_type( 'testimonials', $args );
}

add_action( 'init', 'testimonials' );


add_filter( 'woocommerce_register_post_type_product', 'addCoursesProductType' );

function addCoursesProductType( $args ){
	$labels = array(
		'name'               => __( 'Courses', 'skillnet' ),
		'singular_name'      => __( 'Course', 'skillnet' ),
		'menu_name'          => _x( 'Courses', 'Admin menu name', 'skillnet' ),
		'add_new'            => __( 'Add Course', 'skillnet' ),
		'add_new_item'       => __( 'Add New Course', 'skillnet' ),
		'edit'               => __( 'Edit', 'skillnet' ),
		'edit_item'          => __( 'Edit Course', 'skillnet' ),
		'new_item'           => __( 'New Course', 'skillnet' ),
		'view'               => __( 'View Course', 'skillnet' ),
		'view_item'          => __( 'View Course', 'skillnet' ),
		'search_items'       => __( 'Search Courses', 'skillnetskillnet' ),
		'not_found'          => __( 'No Courses found', 'skillnet' ),
		'not_found_in_trash' => __( 'No Courses found in trash', 'skillnet' ),
		'parent'             => __( 'Parent Course', 'skillnet' )
	);

	$args['labels'] = $labels;
	$args['description'] = __( 'This is where you can add new courses to your store.', 'skillnet' );
	return $args;
}


function apprenticeships() {
	$labels = array(
		'name'               => _x( 'Apprenticeships', 'apprenticeships' ),
		'singular_name'      => _x( 'Apprenticeship', 'apprenticeship' ),
		'add_new'            => _x( 'Add New', 'apprenticeship' ),
		'add_new_item'       => __( 'Add New Apprenticeship' ),
		'edit_item'          => __( 'Edit Apprenticeship' ),
		'new_item'           => __( 'New Apprenticeship' ),
		'all_items'          => __( 'All Apprenticeships' ),
		'view_item'          => __( 'View Apprenticeships' ),
		'search_items'       => __( 'Search Apprenticeships' ),
		'not_found'          => __( 'No Apprenticeships found' ),
		'not_found_in_trash' => __( 'No Apprenticeships found in the Trash' ),
		'parent_item_colon'  => '',
		'menu_name'          => 'Apprenticeships',
	);
	$args   = array(
		'labels'        => $labels,
		'description'   => 'Holds our Apprenticeships',
		'public'        => true,
		'menu_icon'     => 'dashicons-welcome-add-page',
		'menu_position' => 7,
		'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'author' ),
		'has_archive'   => true,
	);
	register_post_type( 'apprenticeships', $args );
}

add_action( 'init', 'apprenticeships' );

function testimonials_tax() {
	$labels = array(
		'name'              => _x( 'Testimonial Categories', 'taxonomy general name' ),
		'singular_name'     => _x( 'Testimonial Category', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Categories' ),
		'all_items'         => __( 'All Categories' ),
		'parent_item'       => __( 'Parent Category' ),
		'parent_item_colon' => __( 'Parent Category:' ),
		'edit_item'         => __( 'Edit Category' ),
		'update_item'       => __( 'Update Category' ),
		'add_new_item'      => __( 'Add New Category' ),
		'new_item_name'     => __( 'New Category' ),
		'menu_name'         => __( 'Categories' ),
	);
	$args   = array(
		'labels'       => $labels,
		'hierarchical' => true,
		'query_var'    => true,
	);
	register_taxonomy( 'testimonial_category', 'testimonials', $args );
}

add_action( 'init', 'testimonials_tax', 0 );

function generateRandomString( $length = 10 ) {
	$characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen( $characters );
	$randomString     = '';
	for ( $i = 0; $i < $length; $i++ ) {
		$randomString .= $characters[ wp_rand( 0, $charactersLength - 1 ) ];
	}
	return $randomString;
}

function start_hero_slider() {
	?>
	<script>
		$(document).ready(function(){
			$('.slick-hero').slick({
				autoplay: true,
				autoplaySpeed: 6000,
				prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-left"></i></button>',
				nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-right"></i></button>',
				asNavFor: '.header-slider',

			});
		});
		$(document).ready(function(){
			$('.header-slider').slick({
				arrows: false,
				draggable: false,
			});
		});
	</script>
	<?php
}

function start_slick_slider( $requested_post_type ) {

	?>
	<script>
		jQuery(document).ready(function($){
			$(".slick-cards").each( function()
			{
				$(this).slick({
					prevArrow: '<button type="button" class="cards-prev"><i class="fas fa-arrow-left"></i></button>',
					nextArrow: '<button type="button" class="cards-next"><i class="fas fa-arrow-right"></i></button>',
					appendArrows: '#cards-arrows',
					slidesToShow: 4,
					responsive: [
						{
							breakpoint: 800,
							settings: {
								slidesToShow: 1,
							}
						}
					],
				});

			});


		});
	</script>
	<?php
};
