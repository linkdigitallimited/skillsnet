<div class="layout-grid" style="grid-template-columns: repeat(<?php echo count( get_sub_field( 'grid_items' ) ); ?>, 1fr)">
	<?php foreach ( get_sub_field( 'grid_items' ) as $item ) { ?>
		<a href="<?php echo $item['link']; ?>" class="grid-item">
			<img src="<?php echo $item['image']; ?>" alt="<?php echo $item['content']; ?>">
			<h4 class="tile__title"><?php echo $item['content']; ?></h4>
		</a>
	<?php } ?>
</div>
