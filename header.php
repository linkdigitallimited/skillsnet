<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package skillsnet
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_enqueue_script( 'jquery' ); ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'skillsnet' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="top-bar">
			<div class="site-branding">
				<a href="/"><img src="http://skillsnet-local.linkstaging.co.uk/wp-content/uploads/2021/04/skillnetlogo-small.png" alt="Skillnet Logo"></a>
			</div><!-- .site-branding -->

			<nav id="site-navigation" class="main-navigation">
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					)
				);
				?>
			</nav><!-- #site-navigation -->
			<nav class="login-menu">
				<ul>
					<li><a href="#"><i class="fas fa-user"></i> Login</a></li>
					<li class="toggle"><i class="fas fa-bars"></i></li>
				</ul>
			</nav>
			<div class="menu-toggle toggle">
				<i class="fas fa-bars fa-2x"></i>
			</div>
		</div>
		<nav id="sidebar-menu">
			<div id="close">
				<i class="fas fa-times"></i>
			</div>
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-2',
					'menu_id'        => 'sidebar_menu',
				)
			);
			?>
			<a href="#" class="login-item"><i class="fas fa-user"></i> Login</a>
			<div class="social-icons">
				<a href="#"><i class="fab fa-twitter"></i></a>
				<a href="#"><i class="fab fa-facebook-f"></i></a>
				<a href="#"><i class="fab fa-instagram"></i></a>
				<a href="#"><i class="fab fa-linkedin-in"></i></a>
			</div>
		</nav>

	</header><!-- #masthead -->
