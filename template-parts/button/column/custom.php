<?php
	$button = $args['button'];
?>

<a href="<?php echo $button['url']; ?>" class="button button--<?php echo $button['style']; ?>"><?php echo $button['text']; ?></a>
