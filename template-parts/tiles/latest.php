<?php

$tile_posts = new WP_Query(
	array(
		'post_type'        => $requested_post_type,
		'posts_per_page'   => 1,
		'category__not_in' => 3,
	)
);
if ( $tile_posts->have_posts() ) :
	while ( $tile_posts->have_posts() ) :
		$tile_posts->the_post();
		get_template_part( 'template-parts/tiles/homepage' );
endwhile;
else :
	// Insert any content or load a template for no posts found.
	echo 'No posts found';
endif;

wp_reset_query();
?>
<div class="tile-grid-secondary">
	<?php
	$tile_posts_secondary = new WP_Query(
		array(
			'post_type'        => $requested_post_type,
			'posts_per_page'   => 3,
			'offset'           => 1,
			'category__not_in' => 3,
		)
	);
	if ( $tile_posts_secondary->have_posts() ) :
		while ( $tile_posts_secondary->have_posts() ) :
			$tile_posts_secondary->the_post();
			get_template_part( 'template-parts/tiles/homepage-secondary' );
		endwhile;
	else :
		// Insert any content or load a template for no posts found.
		echo 'No posts found';
	endif;
	wp_reset_query();
