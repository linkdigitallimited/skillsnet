<?php
$id = $args['id'];
?>

<a class="tile-item" href="<?php echo get_the_permalink( $id ); ?>">
	<div class="tile__thumbnail">
		<?php echo get_the_post_thumbnail( $id ); ?>
	</div>
	<h4 class="tile__title">
		<?php echo get_the_title( $id ); ?>
	</h4>
</a>
