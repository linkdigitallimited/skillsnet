<?php
/**
 * Template for displaying the Updates
 */

$post_type  = get_sub_field( 'post_type' );
$image_side = get_sub_field( 'image_side' );
$type       = get_sub_field( 'type' );

$updates = new WP_Query(
	array(
		'post_type'      => $post_type,
		'posts_per_page' => 1,
		'category__in'   => 3,
	)
); ?>
<div class="<?php echo get_sub_field( 'background' ); ?> <?php echo get_sub_field( 'padding' ); ?>">
	<div class="container updates__grid updates__grid--<?php echo $image_side; ?>">
		<?php
		if ( 'selected' === $type ) {
			if ( $updates->have_posts() ) :
				while ( $updates->have_posts() ) :
					$updates->the_post();
					?>
				<div class="updates__thumbnail" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')"></div>
				<div class="updates__title">
					<h2 class="section-title"><?php echo get_sub_field( 'title' ); ?></h2>
				</div>
				<div class="updates__holder">
					<div class="updates__content">
						<?php echo get_the_content(); ?>
					</div>
				</div>
					<?php
			endwhile;
		else :
			// Insert any content or load a template for no posts found.
		endif;

			wp_reset_query();
		} elseif ( 'text' ) {
			?>
			<div class="updates__thumbnail" style="background-image: url('<?php echo get_sub_field( 'image' ); ?>')"></div>
			<div class="updates__title">
				<h2 class="section-title"><?php echo get_sub_field( 'title' ); ?></h2>
			</div>
			<div class="updates__holder">
				<div class="updates__content">
					<?php echo get_sub_field( 'content' ); ?>

					<?php if ( get_sub_field( 'button_type' ) !== 'none' ) { ?>
						<?php
						get_template_part( 'template-parts/button/' . get_sub_field( 'button_type' ), '' );
						?>
					<?php } ?>
				</div>
			</div>
			<?php
		}
		?>

	</div>
</div>
