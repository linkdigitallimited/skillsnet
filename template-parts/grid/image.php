<div class="layout-grid" style="grid-template-columns: repeat(<?php echo count( get_sub_field( 'grid_items' ) ); ?>, 1fr)">
	<?php foreach ( get_sub_field( 'grid_items' ) as $item ) { ?>
		<img src="<?php echo $item['image']; ?>" alt="">
	<?php } ?>
</div>
