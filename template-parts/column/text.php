<?php $column = $args['column']; ?>

<div class="column__content">
	<h2 class="section-title"><?php echo $column['column_title']; ?></h2>
	<?php echo $column['column_content']; ?>
	<?php if ( 'none' !== $column['column_button']['button_type'] ) { ?>
		<?php
		get_template_part( 'template-parts/button/column/' . $column['column_button']['type'], '', array( 'button' => $column['column_button'] ) );
		?>
	<?php } ?>
</div>
