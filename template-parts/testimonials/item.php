<?php
/**
 * Template for displaying Testimonials items
 */
?>

<div class="quote-mark">
	<i class="fas fa-quote-left fa-3x"></i>
</div>
<div class="testimonial-content">
	<?php echo get_the_content(); ?>
</div>
<div class="meta-footer">
	<div class="profile-image-outline">
		<img src="<?php echo get_field( 'testimonial_image' ); ?>" alt="<?php echo get_field( 'testimonial_name' ); ?>">
	</div>
	<div class="profile-details">
		<div class="profile-name">
			<?php echo get_field( 'testimonial_name' ); ?>
		</div>
		<div class="profile-position">
			<?php echo get_field( 'testimonial_position' ); ?>
		</div>
	</div>
	<?php if ( get_field( 'testimonial_logo' ) ) { ?>
		<div class="profile-logo">
			<img src="<?php echo get_field( 'testimonial_logo' ); ?>" alt="<?php echo get_the_title(); ?>">
		</div>
	<?php }; ?>
</div>
