<?php
$button = $args['button'];
?>
<div class="button-contact">
	<a href="mailto:support@skillnet.co.uk" class="button button--<?php echo $button['style']; ?>">Enquire Online</a> <p>or call 01923 630 800</p>
</div>
