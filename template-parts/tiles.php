<?php
/**
 * Template for displaying the Tiles
 */
$content_type        = get_sub_field( 'content_type' );
$requested_post_type = get_sub_field( 'post_types' );
$page_title          = get_sub_field( 'title' );
?>
<div class="<?php echo get_sub_field( 'background' ); ?> <?php echo get_sub_field( 'padding' ); ?>">
	<div class="container tiles">
		<?php
		get_template_part(
			'template-parts/section-title',
			'',
			array(
				'page_title'  => $page_title,
			)
		);
		?>
		<div class="tile-grid">
			<?php
				// The Query.
			get_template_part( 'template-parts/tiles/' . $content_type );
			?>
			<a class="tile-item secondary-tile-item" href="<?php echo get_post_type_archive_link( $requested_post_type ); ?>">
				<div class="tile__thumbnail">
					<img src="http://skillsnet.local/wp-content/uploads/2021/04/ksenia-kudelkina-o5_LYQ44gsM-unsplash-1.png" alt="">
				</div>
				<h4 class="tile__title">
					View all <?php echo $requested_post_type; ?>
				</h4>
			</a>
			</div>
		</div>
		<div class="mobile-button">
			<?php if ( get_sub_field( 'button_type' ) !== 'none' ) { ?>
				<?php
				get_template_part( 'template-parts/button/' . get_sub_field( 'button_type' ), '' );
				?>
			<?php } ?>
		</div>
	</div>
</div>
