<?php

/**
 * Template for displaying the Cards
 */
$content_type        = get_sub_field( 'content_type' );
$requested_post_type = get_sub_field( 'post_types' );
$page_title          = get_sub_field( 'title' );

?>
<div class="<?php echo get_sub_field( 'background' ); ?> <?php echo get_sub_field( 'padding' ); ?>">
	<div class="container cards">
		<?php
		get_template_part(
			'template-parts/section-title',
			'',
			array(
				'page_title'  => $page_title,
			)
		);
		?>
		<?php if ( 'slider' === get_sub_field( 'card_type' ) ) { ?>
		<div class="<?php echo $content_type . ' ' . $requested_post_type; ?> cards-grid slick-cards slick-cards-<?php echo $requested_post_type; ?>">
			<?php
			// The Query.
			if ( 'latest' === $content_type ) {
				$cards_posts = new WP_Query(
					array(
						'post_type'        => $requested_post_type,
						'posts_per_page'   => 16,
						'category__not_in' => 3,
					)
				);
				if ( $cards_posts->have_posts() ) :
					while ( $cards_posts->have_posts() ) :
						$cards_posts->the_post();
						get_template_part( 'template-parts/cards/homepage' );
					endwhile;
				else :
					// Insert any content or load a template for no posts found.
				endif;

				wp_reset_query();
			} else {
				$cards_posts = get_sub_field( 'selected_posts' );
				foreach ( $cards_posts as $cards_post ) {
					get_template_part( 'template-parts/cards/homepage', '', array( 'id' => $cards_post ) );

				}
			}

			?>
		</div>
		<div id="cards-arrows"></div>
	</div>
		<?php } elseif ( 'cards' === get_sub_field( 'card_type' ) ) { ?>
		<div class="cards-grid cards-grid--standard">
			<?php
			// The Query.
			if ( 'latest' === $content_type ) {
				$cards_posts = new WP_Query(
					array(
						'post_type'        => $requested_post_type,
						'posts_per_page'   => -1,
						'category__not_in' => 3,
					)
				);
				if ( $cards_posts->have_posts() ) :
					while ( $cards_posts->have_posts() ) :
						$cards_posts->the_post();
						get_template_part( 'template-parts/cards/homepage' );
					endwhile;
				else :
					// Insert any content or load a template for no posts found.
				endif;

				wp_reset_query();
			} else {
				$cards_posts = get_sub_field( 'selected_posts' );
				foreach ( $cards_posts as $cards_post ) {
					get_template_part( 'template-parts/cards/homepage', '', array( 'id' => $cards_post ) );

				}
			}
			?>
			</div>
			<?php
		}
		?>
	<div class="mobile-button">
		<?php if ( get_sub_field( 'button_type' ) !== 'none' ) { ?>
			<?php
			get_template_part( 'template-parts/button/' . get_sub_field( 'button_type' ), '' );
			?>
		<?php } ?>
	</div>
</div>

<?php

add_action( 'wp_footer', 'start_slick_slider', 40, 1 );
