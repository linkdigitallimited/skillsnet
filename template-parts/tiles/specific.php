<?php
$tile_posts = get_sub_field( 'selected_posts' );
get_template_part( 'template-parts/tiles/homepage', '', array( 'id' => $tile_posts[0] ) );
array_shift( $tile_posts );
?>
<div class="tile-grid-secondary">
	<?php
	foreach ( $tile_posts as $tile_post ) {
		get_template_part( 'template-parts/tiles/homepage-secondary', '', array( 'id' => $tile_post ) );
	}
	?>
</div>
