<?php
/**
 * Template for displaying the section titles
 */
	$page_title = $args['page_title'];
	$button = $args['button'];
	$button_url = $args['button_url'];
	$button_text = $args['button_text'];
?>
<div class="flex-title">
	<h2 class="section-title"><?php echo $page_title; ?></h2>
	<?php if ( get_sub_field( 'button_type' ) !== 'none' ) { ?>
		<?php
		get_template_part( 'template-parts/button/' . get_sub_field( 'button_type' ), '' );
		?>
	<?php } ?>
</div>
