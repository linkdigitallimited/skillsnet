<div class="container faq-grid">
	<div class="faq__image" style="background-image: url('<?php echo get_sub_field( 'image' ); ?>')"></div>
	<div><h2 class="section-title"><?php echo get_sub_field( 'title' ); ?></h2>
	<?php
	$i = 1;
	foreach ( get_sub_field( 'questions' ) as $question ) {
		?>
			<div class="faq__item<?php echo $i === 1 ? ' active' : ''; ?>">
				<div class="faq__question">
					<h3><?php echo $question['question']; ?></h3>
					<i class="fas fa-plus"></i>
				</div>
				<div class="faq__answer">
					<?php echo $question['answer']; ?>
				</div>
			</div>
		<?php
		$i++;
	}
	?>
	</div>
</div>
