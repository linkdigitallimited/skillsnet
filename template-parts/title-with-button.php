<div class="container title-container">
	<div class="title-holder">
		<h2 class="section-title"><?php echo get_sub_field( 'title' ); ?></h2>
		<div class="content--holder"><?php echo get_sub_field( 'content' ); ?></div>
	</div>
	<?php if ( get_sub_field( 'button_type' ) !== 'none' ) { ?>
		<?php
		get_template_part( 'template-parts/button/' . get_sub_field( 'button_type' ), '' );
		?>
	<?php } ?>
</div>
