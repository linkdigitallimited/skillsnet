<div class="<?php echo get_sub_field( 'background' ); ?> <?php echo get_sub_field( 'padding' ); ?>">
	<div class="container grid-<?php echo get_sub_field( 'grid_type' ) ?>">
		<?php if ( get_sub_field( 'title' ) ) { ?>
			<h2 class="section-title title--<?php echo get_sub_field( 'title_alignment' ); ?>"><?php echo get_sub_field( 'title' ); ?></h2>
		<?php } ?>
		<?php get_template_part( 'template-parts/grid/' . get_sub_field( 'grid_type' ) ); ?>
	</div>
</div>
