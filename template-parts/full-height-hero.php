<?php
/**
 * Template for displaying the Full Height Hero
 */

$images   = get_sub_field( 'background_images' );
$headings = get_sub_field( 'headings' );
?>

<div class="full-height-hero--content">
	<?php if ( 1 == count( $headings ) ) { ?>
		<div class="content--holder">
			<h2><?php echo $headings['0']['subheading_text']; ?></h2>
			<h1><?php echo $headings['0']['heading_text']; ?></h1>
			<div class="hero__button">
				<?php if ( 'video' === $headings['0']['button_style'] ) { ?>
					<a href="<?php echo $headings['0']['video_url']; ?>" class="hero__video-link">
						<span class="fa-stack fa-2x">
							<i class="fas fa-circle fa-stack-2x"></i>
							<i class="fas fa-play fa-stack-1x fa-inverse"></i>
						</span>
						Watch our video</i>
					</a>
				<?php } else { ?>
					<a href="<?php echo $headings['0']['button_url']; ?>" class="button"><?php echo $headings['0']['button_text']; ?></a>
				<?php } ?>
			</div>
		</div>
	<?php } else { ?>
		<div class="content--holder header-slider">
			<?php foreach ( $headings as $heading ) { ?>
				<div>
					<h2><?php echo $heading['subheading_text']; ?></h2>
					<h1><?php echo $heading['heading_text']; ?></h1>
					<div class="hero__button">
						<?php if ( 'video' === $heading['button_style'] ) { ?>
							<a href="<?php echo $heading['video_url']; ?>" class="hero__video-link">
								<span class="fa-stack fa-2x">
									<i class="fas fa-circle fa-stack-2x"></i>
									<i class="fas fa-play fa-stack-1x fa-inverse"></i>
								</span>
								Watch our video</i>
							</a>
						<?php } else { ?>
							<a href="<?php echo $heading['button_url']; ?>" class="button"><?php echo $heading['button_text']; ?></a>
						<?php } ?>
					</div>
				</div>
			<?php } ?>
		</div>
	<?php } ?>



</div>
<?php
if ( 1 == count( $images ) ) {
	?>
		<div class="full-height-hero--background" style="background-image: url('<?php echo $images[0]['image']; ?>')">

		</div>
	 <?php
} else {
	?>
		<div class="full-height-hero--background" style="background-color: #0f834d">
			<div class="slick-hero">
				<?php
				foreach ( $images as $image ) {
					echo "<div><img src=' " . $image['image'] . "'></div>";
				}
				?>
			</div>

		</div>
	<?php
}

add_action( 'wp_footer', 'start_hero_slider', 40 );
