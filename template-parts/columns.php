<?php
/* layout for columns */
$columns = get_sub_field( 'columns' );
?>
<div class="<?php echo get_sub_field( 'background' ); ?> <?php echo get_sub_field( 'padding' ); ?>">
	<div class="container columns__grid" style="grid-template-columns: repeat(<?php echo count( get_sub_field( 'columns' ) ); ?>, 1fr)">
		<?php
		foreach ( $columns as $column ) {
			get_template_part(
				'template-parts/column/' . $column['column_type'],
				'',
				array(
					'column'  => $column,
				)
			);
		}
		?>
	</div>
</div>
