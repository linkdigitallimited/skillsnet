<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package skillsnet
 */

?>

	<footer id="colophon" class="site-footer">
		<a href="#" class="footer-highlight">
			<h2>Ready to get started? <br class="mobile-break"><span class="highlight-title">Lets work together</span></h2>
			<div class="footer-line"></div>
			<span class="footer-circle">
			  <i class="fas fa-arrow-right fa-3x"></i>
			</span>
		</a>
		<div class="container footer-grid">
			<div>
				<h3>SKILLNET</h3>
				<p>info@skillnet.org.uk <br> 01923 630 800<br> Unit 4, Woodshot Meadow<br> Watford, Hertfordshire, WD 18 8YS</p>
				<div class="social-icons">
					<a href="#"><i class="fab fa-twitter"></i></a>
					<a href="#"><i class="fab fa-facebook-f"></i></a>
					<a href="#"><i class="fab fa-instagram"></i></a>
					<a href="#"><i class="fab fa-linkedin-in"></i></a>
				</div>
			</div>
			<div>
				<h3 class="footer-title">Navigation</h3>
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'footer-1',
						'menu_id'        => 'footer-nav-menu',
					)
				);
				?>
			</div>
			<div>
				<h3 class="footer-title">Policies</h3>
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'footer-2',
						'menu_id'        => 'footer-policies-menu',
					)
				);
				?>
			</div>
			<div>
				<h3 class="footer-title">Email Newsletter</h3>
				<?php echo do_shortcode( '[contact-form-7 id="125" title="Newsletter Signup"]' ); ?>
			</div>
		</div>
		<div class="footer-copyright">
			<div class="container copyright-level">
				<div class="copyright">
					<i class="fas fa-copyright"></i> 2021. Skillnet. All Rights Reserved.
				</div>
				<div class="credit">
					Website by <a href="https://www.linkdigital.co.uk/" class="highlight-footer">Link Digital</a>
				</div>
				<a href="#masthead" class="back-to-top">
					<i class="fas fa-arrow-up"></i>
				</a>

			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->
<?php wp_footer(); ?>
<script>
	toggler = document.querySelectorAll('.toggle');
	sidebar = document.getElementById('sidebar-menu');
	closeButton = document.getElementById('close');

	toggler.forEach(item => {
		item.addEventListener('click', event => {
			sidebar.style.right = '0'
		})
	})
	closeButton.addEventListener('click', event => {
		sidebar.style.right = '-60vw'
	})
</script>

<script>
	questions = document.querySelectorAll('.faq__item');

	questions.forEach(question => {
		question.addEventListener('click', event => {
			questions.forEach.call(questions, function (el) {
				el.classList.remove("active");
			});
			question.classList.add("active")
		})
	})
</script>

</body>
</html>
