<?php
$id           = $args['id'];
$content_post = get_post( $id );
$content      = $content_post->post_content;
$content      = apply_filters( 'the_content', $content );
$content      = str_replace( ']]>', ']]&gt;', $content );
?>
<a class="cards-item" href="<?php echo get_the_permalink( $id ); ?>">

	<div class="cards-thumbnail">
		<?php echo get_the_post_thumbnail( $id ); ?>
	</div>
	<div class="cards__content">
		<h4 class="cards__title">
			<?php echo get_the_title( $id ); ?>
		</h4>
		<div class="cards__ft-text">
			<?php echo $content; ?>
		</div>
	</div>
</a>
